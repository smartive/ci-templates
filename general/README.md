# general

## [base](./base.yml)

Contains templates that are relevant for all other templates
or pipelines.

### .base

Base tagged job for smartive runner.

## [build](./build.yml)

Templates for building on different environments and / or
technologies.

### .gcb-docker

Perform a google cloud build (GCB) build. This template does
perform the basic gcb login (with a pre-defined
`$GCB_KEY` variable or file), creates a build command out of
environment variables (that starts with `SUB_`) and executes
the build. The cloudbuild file needs to be created.

#### config / overwrites

- `CLOUDBUILD`: location from the root of the repo to the used
  cloudbuild yaml file.
- `SUB_`: all environment variables that start with `SUB_` are
  used as substitutions in the cloud build.

### .gitlab-docker

Template for building a docker image and publish it to the local
gitlab registry. This is mostly used for our private projects
since they don't have access to the google cloud builder.

### .gitlab-docker-to-gcr

Template for building a docker image and publish it to the
Google Cloud registry.

#### config / overwrites

- `DOCKER_FILE` (default: `./Dockerfile`): The location of the
  dockerfile that should be used.
- `DOCKER_TAG` (default: `$CI_REGISTRY_IMAGE:latest`):
  The tag that should be used to create the image.
- `DOCKER_CONTEXT` (default: `.`): The docker build context.

## [release](./release.yml)

Templates for releasing with the aid of `semantic-release`.

### .create-release

Automatically create a release (tag / release publish) with
`semantic-release` and publish the results. Depending on the
configuration resided in the repository with `.releaserc.json`
or the `release` property in the `package.json` file, the result
may vary (published on gitlab / github / docker /
npm / whatever).

### .create-manual-release

Like `.create-release` but contains the `when: manual`
instruction, so that the deverloper can decide when to create
releases.
