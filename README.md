# ci-templates

This repository contains files that can / should be included
into other ci pipelines and should ease up the gitlab config.

The usage is documented in markdown files.

## Naming

If a `yml` file is directly in a folder, the name of the file
describes the general subject of the templates that are contained
in the files. If a directory contains a directory named
`defaults`, the files that reside in there are for defaulting
whole pipelines.

## General Usage

To include a file within gitlab ci use:

```yaml
include:
  - project: 'smartive/ci-templates'
    ref: MAJOR-BRANCH (e.g. 4.x)
    file: <path within this repo>
```

The template files contain keys with a leading full-stop.
They are not executed with seen in gitlab-ci. They
can be used to create custom pipelines and function as base
tasks to be extended.

As an example:

```yaml
include:
  - project: 'smartive/ci-templates'
    ref: MAJOR-BRANCH (e.g. 4.x)
    file: /all-templates.yml

build my docker image:
  extends: .gitlab-docker
  variables:
    DOCKER_CONTEXT: ./ctx
```

## Content

- [cloud-run](./cloud-run) - templates for cloud run deployments
- [general](./general) - contains general templates that are
  used for basic building
- [gitops](./gitops) - contains gitops specific templates (argocd)
- [preview](./preview) - contains templates for preview builds
- [renovate](./renovate) - contains templates for renovate bot triggers
- [terraform](./terraform) - contains templates for terraform builds
- [all-templates](./all-templates.yml) - imports all templated
  elements in this repository (so that all templates are
  available)
