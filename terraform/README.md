# terraform

## [deploy](./deploy.yml)

Contains templates that are relevant for deploying infrastructure
with terraform.

### .tf-deploy

Take a terraform definition folder and deploy it with
auto-approve. To add variable values for terraform, use
`TF_VAR_xxxxx` environment variables.

#### config / overwrites

- `DIR` (default: `./`): The directory that contains the terraform files.
- `GOOGLE_APPLICATION_CREDENTIALS`: Credentials that the terraform apply will use to deploy the files. This should be set within the gitlab group.

## [test](./test.yml)

Contains templates that are relevant for testing (validate) terraform files.

### .tf-test

Take a terraform definition folder and test it with
`terraform validate`.

#### config / overwrites

- `DIR` (default: `./`): The directory that contains the terraform files.
